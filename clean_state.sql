CREATE TABLE IF NOT EXISTS `list_admin_status` (
  `admin_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_status` varchar(50) NOT NULL,
  PRIMARY KEY (`admin_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `list_admin_status` (`admin_status_id`, `admin_status`) VALUES
	(1, 'Aktif'),
	(2, 'Tidak Aktif'),
	(3, 'Banned');

CREATE TABLE IF NOT EXISTS `list_division` (
  `division_id` int(11) NOT NULL AUTO_INCREMENT,
  `division_name` varchar(50) NOT NULL,
  PRIMARY KEY (`division_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `list_division` (`division_id`, `division_name`) VALUES
	(1, 'System Administrator'),
	(2, 'Staff');


-- Dumping structure for table template_lte.list_access_control
CREATE TABLE IF NOT EXISTS `list_access_control` (
  `admin_tier_id` int(11) NOT NULL AUTO_INCREMENT,
  `access_level` int(11) NOT NULL,
  `access_divisionId` int(11) NOT NULL,
  `access_levelName` varchar(50) NOT NULL,
  PRIMARY KEY (`admin_tier_id`),
  KEY `FK__list_division` (`access_divisionId`),
  CONSTRAINT `FK__list_division` FOREIGN KEY (`access_divisionId`) REFERENCES `list_division` (`division_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table template_lte.list_access_control: ~0 rows (approximately)
/*!40000 ALTER TABLE `list_access_control` DISABLE KEYS */;
INSERT INTO `list_access_control` (`admin_tier_id`, `access_level`, `access_divisionId`, `access_levelName`) VALUES
	(1, 100, 1, 'Super Admin'),
	(1, 90, 2, 'Staff');
/*!40000 ALTER TABLE `list_access_control` ENABLE KEYS */;

-- Dumping structure for table template_lte.list_admin
CREATE TABLE IF NOT EXISTS `list_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(125) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_email` varchar(80) NOT NULL,
  `admin_phone` varchar(20) NOT NULL,
  `admin_lastLogin` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `admin_statusId` int(11) NOT NULL,
  `admin_tierId` int(11) NOT NULL,
  PRIMARY KEY (`admin_id`),
  KEY `FK_list_admin_list_admin_status` (`admin_statusId`),
  KEY `FK_list_admin_list_access_control` (`admin_tierId`),
  CONSTRAINT `FK_list_admin_list_access_control` FOREIGN KEY (`admin_tierId`) REFERENCES `list_access_control` (`admin_tier_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_list_admin_list_admin_status` FOREIGN KEY (`admin_statusId`) REFERENCES `list_admin_status` (`admin_status_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table template_lte.list_admin: ~1 rows (approximately)
/*!40000 ALTER TABLE `list_admin` DISABLE KEYS */;
INSERT INTO `list_admin` (`admin_id`, `admin_name`, `admin_password`, `admin_email`, `admin_phone`, `admin_lastLogin`, `created_date`, `updated_date`, `admin_statusId`, `admin_tierId`) VALUES
	(1, 'Sanjaya', '$2y$10$QEo45sr7r.Cus/cepy9WgOIuCbRwMYkdvcATGoP9xsN2EHXYUA2EK', 'admin@gmail.com', '08', NULL, NULL, NULL, 1, 1);

-- Dumping structure for table template_lte.list_session_token
CREATE TABLE IF NOT EXISTS `list_session_token` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `session_token` varchar(100) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `active_time` datetime NOT NULL,
  `expire_time` datetime NOT NULL,
  `is_login` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`session_id`),
  KEY `FK__list_admin` (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Added date 9 Maret 2021
CREATE TABLE IF NOT EXISTS `list_action` (
  `action_id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(100) NOT NULL,
  `action_code` varchar(100) NOT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `list_action` (`action_id`, `action`, `action_code`, `description`) VALUES
	(1, 'Login', '001L', NULL),
	(2, 'Logout', '001LG', NULL),
	(3, 'Add Admin', '002AA', NULL),
	(4, 'Edit Admin', '002EA', NULL),
	(5, 'View UAC Management', '003U', NULL),
	(6, 'Add Position', '003UAP', NULL),
	(7, 'Edit Position', '003UEP', NULL),
	(8, 'View Detail Position', '003UDP', NULL),
	(9, 'Change Access Position', '003UCHP', NULL),
	(10, 'Add Division', '003UAD', NULL),
	(11, 'Edit Division', '003UED', NULL),
	(12, 'View Admin Management', '002A', NULL),
	(13, 'View Jenis Management', '003J', NULL),
	(14, 'Add Jenis Kendaraan', '003JA', NULL),
	(15, 'Edit Jenis Kendaraan', '003JE', NULL),
	(16, 'Delete Jenis Kendaraan', '003JD', NULL),
	(17, 'View Merk Management', '004M', NULL),
	(18, 'Add Merk', '004MA', NULL),
	(19, 'Edit Merk', '004ME', NULL),
	(20, 'Delete Merk', '004MD', NULL),
	(21, 'View Service Management', '005S', NULL),
	(22, 'Add Service', '005SA', NULL),
	(23, 'Edit Service', '005SE', NULL),
	(24, 'Delete Service', '005SD', NULL),
	(25, 'View Kendaraan Management', '006K', NULL),
	(26, 'Add Kendaraan', '006KA', NULL),
	(27, 'Edit Kendaraan', '006KE', NULL),
	(28, 'View Service Kendaraan Management', '007SK', NULL),
	(29, 'Add Service Kendaraan', '007SKA', NULL),
	(30, 'Edit Service Kendaraan', '007SKE', NULL),
	(31, 'Delete Service Kendaraan', '007SKD', NULL),
	(32, 'Delete Kendaraan', '006KD', NULL);

CREATE TABLE IF NOT EXISTS `list_activity` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `action_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `platform` text DEFAULT NULL,
  `browser` text NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `time` datetime NOT NULL,
  `description` text DEFAULT NULL,
  `meta_data` mediumtext DEFAULT NULL,
  PRIMARY KEY (`activity_id`),
  KEY `FK_list_activity_list_action` (`action_id`),
  KEY `admin_id` (`admin_id`),
  CONSTRAINT `FK_list_activity_list_action` FOREIGN KEY (`action_id`) REFERENCES `list_action` (`action_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `list_admin`
	ADD COLUMN `admin_photo` TEXT NULL DEFAULT NULL AFTER `admin_tierId`;

CREATE TABLE IF NOT EXISTS `list_admin_module` (
  `rel_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_tier_id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  `assign_date` datetime NOT NULL,
  `assign_by` int(11) NOT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `FK_list_admin_module_list_access_control` (`admin_tier_id`),
  KEY `FK_list_admin_module_list_action` (`action_id`),
  KEY `FK_list_admin_module_list_admin` (`assign_by`),
  CONSTRAINT `FK_list_admin_module_list_access_control` FOREIGN KEY (`admin_tier_id`) REFERENCES `list_access_control` (`admin_tier_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_list_admin_module_list_action` FOREIGN KEY (`action_id`) REFERENCES `list_action` (`action_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_list_admin_module_list_admin` FOREIGN KEY (`assign_by`) REFERENCES `list_admin` (`admin_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tbl_jenis` (
  `jenis_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(75) NOT NULL,
  PRIMARY KEY (`jenis_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tbl_merk` (
  `merk_id` int(11) NOT NULL AUTO_INCREMENT,
  `merk` varchar(50) NOT NULL,
  PRIMARY KEY (`merk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tbl_service` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `service` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tbl_kendaraan` (
  `kendaraan_id` int(11) NOT NULL AUTO_INCREMENT,
  `merk_id` int(11) NOT NULL,
  `jenis_id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kilometer` int(11) NOT NULL,
  `warna` varchar(20) NOT NULL,
  `thn_kendaraan` year(4) NOT NULL,
  `tgl_beli` date NOT NULL,
  `no_polisi` varchar(10) NOT NULL,
  `tgl_pajak` date NOT NULL,
  `stnk` date NOT NULL,
  `pic` varchar(100) NOT NULL,
  PRIMARY KEY (`kendaraan_id`),
  KEY `FK__tbl_merk` (`merk_id`),
  KEY `FK__tbl_jenis` (`jenis_id`),
  CONSTRAINT `FK__tbl_jenis` FOREIGN KEY (`jenis_id`) REFERENCES `tbl_jenis` (`jenis_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK__tbl_merk` FOREIGN KEY (`merk_id`) REFERENCES `tbl_merk` (`merk_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tbl_kendaraan_service` (
  `rel_id` int(11) NOT NULL AUTO_INCREMENT,
  `kendaraan_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `service_date` date NOT NULL,
  `harga` int(11) NOT NULL,
  `file` text NOT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `FK__tbl_kendaraan` (`kendaraan_id`),
  KEY `FK__tbl_service` (`service_id`),
  CONSTRAINT `FK__tbl_kendaraan` FOREIGN KEY (`kendaraan_id`) REFERENCES `tbl_kendaraan` (`kendaraan_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK__tbl_service` FOREIGN KEY (`service_id`) REFERENCES `tbl_service` (`service_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;