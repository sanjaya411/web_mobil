<div class="form-group">
  <label><?= $label ?></label>
  <input type="<?= $type ?? "text" ?>" class="form-control" name="<?= $name ?>" <?= $id_custom ?? "" ?>>
</div>