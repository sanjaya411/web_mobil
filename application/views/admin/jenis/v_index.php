<div class="container-fluid dashboard">
  <?= $breadcrumb ?>
  <h4>Jenis Kendaraan</h4>

  <div class="card mt-3">
    <div class="card-body">
      <?php if ($allowed_add) : ?>
        <button class="btn btn-success btn-sm mb-3" data-toggle="modal" data-target="#modalAddJenisKendaraan">Tambah Jenis Kendaraan</button>
      <?php endif; ?>
      <table class="table table-bordered table-hover w-100" id="tableJenis">
        <thead>
          <tr>
            <th>No</th>
            <th>Jenis</th>
            <th data-orderable="false">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; ?>
          <?php foreach ($list_jenis as $jenis) : ?>
            <tr>
              <td><?= $no++; ?></td>
              <td><?= $jenis->jenis; ?></td>
              <td>
                <?php if ($allowed_edit) : ?>
                  <button class="btn btn-primary btn-sm" onclick="editJenis('<?= encrypt_url($jenis->jenis_id) ?>', '<?= $jenis->jenis ?>')">Edit</button>
                <?php endif; ?>
                <?php if ($allowed_delete) : ?>
                  <button class="btn btn-danger btn-sm" onclick="deleteJenis('<?= encrypt_url($jenis->jenis_id) ?>')">Hapus</button>
                <?php endif; ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php if ($allowed_add) : ?>
  <div class="modal fade" id="modalAddJenisKendaraan">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <?= form_open("/jenis-kendaraan/create-jenis") ?>

          <?php $this->load->view("components/input-group", ["label" => "Jenis Kendaraan", "name" => "jenis"]); ?>

          <button class="btn btn-success btn-sm mt-2" type="submit">Tambah Jenis</button>
          </form>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
<?php endif; ?>

<?php if ($allowed_edit) : ?>
  <div class="modal fade" id="modalEditJenisKendaraan">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4>Edit Jenis Kendaraan</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <?= form_open("/jenis-kendaraan/edit-jenis") ?>
          <input type="hidden" name="jenis_id" id="inputHiddenJenisID">

          <?php $this->load->view("components/input-group", ["label" => "Jenis Kendaraan", "name" => "jenis", "id_custom" => "id='inputEditJenis'"]); ?>

          <button class="btn btn-success btn-sm mt-2" type="submit">Edit Jenis</button>
          </form>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
<?php endif; ?>

<?php if ($allowed_delete) : ?>
  <div class="modal fade" id="modalDeleteJenisKendaraan">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal body -->
        <div class="modal-body">
          <h4>Hapus Jenis Ini ?</h4>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <a href="<?= base_url("jenis-kendaraan") ?>" id="btnDeleteJenis" class="btn btn-danger btn-sm">Hapus Jenis</a>
          <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
<?php endif; ?>

<script>
  let tableJenis = dataTable("tableJenis");

  function editJenis(jenisID, jenis) {
    document.querySelector("#inputHiddenJenisID").value = jenisID;
    document.querySelector("#inputEditJenis").value = jenis;

    $("#modalEditJenisKendaraan").modal("show");
  }

  function deleteJenis(jenisID) {
    document.querySelector("#btnDeleteJenis").href = `<?= base_url("jenis-kendaraan/delete-jenis") ?>/${jenisID}`;

    $("#modalDeleteJenisKendaraan").modal("show");
  }
</script>