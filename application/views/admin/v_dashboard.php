<div class="container-fluid dashboard">
  <?= $breadcrumb; ?>
  <div class="row">
    <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          <h5>Admin Online</h5>
          <hr style="border: 1px solid white;">
          <ul>
            <?php
            foreach ($admin_online as $item) :
              $to_time = strtotime(date("Y-m-d H:i:s"));
              $from_time = strtotime($item->active_time);
              $last_active = round(abs($to_time - $from_time) / 60);
              if ($last_active <= 0) {
                $last_active = "Active";
              } else {
                $last_active .= "m ago";
              }
              echo "<li><a href='" . base_url("admin_detail?id=" . encrypt_url($item->admin_id)) . "'>$item->admin_name</a> $last_active</li>";
            endforeach;
            ?>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          <h5>Data Jenis Kendaraan</h5>
          <h5><?= $count_jenis ?> Unit</h5>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          <h5>Data Merk Kendaraan</h5>
          <h5><?= $count_merk ?> Merk</h5>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card">
        <div class="card-body">
          <h5>Notifikasi Pembayaraan Pajak Kendaraan</h5>
          <?php if (is_array($list_tax_expired) && count($list_tax_expired) > 0) : ?>
            <ul>
              <?php foreach ($list_tax_expired as $tax) : ?>
                <?php
                $now = new DateTime();
                $tax_time = new DateTime($tax->tgl_pajak);
                $interval = $tax_time->diff($now)->d + 1;

                ?>
                <?php if ($now <= $tax_time) : ?>
                  <li><a href="<?= base_url("kendaraan/edit/" . encrypt_url($tax->kendaraan_id)) ?>" class="text-warning">Pajak kendaraan dengan nama <?= $tax->nama ?> no polisi <?= $tax->no_polisi ?> tersisa <?= $interval ?> hari lagi</a></li>
                <?php else : ?>
                  <li><a href="<?= base_url("kendaraan/edit/" . encrypt_url($tax->kendaraan_id)) ?>" class="text-danger">Pajak kendaraan dengan nama <?= $tax->nama ?> no polisi <?= $tax->no_polisi ?> telat dibayar <?= $interval ?> hari</a></li>
                <?php endif; ?>
              <?php endforeach; ?>
            </ul>
          <?php else : ?>
            <p>Tidak ada data</p>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>