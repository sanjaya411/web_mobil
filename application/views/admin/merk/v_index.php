<div class="container-fluid dashboard">
  <?= $breadcrumb ?>
  <h4>Merk Kendaraan</h4>

  <div class="card">
    <div class="card-body">
      <?php if ($allowed_add) : ?>
        <button class="btn btn-success btn-sm mb-3" data-toggle="modal" data-target="#modalAddMerkKendaraan">Tambah Merk</button>
      <?php endif; ?>

      <table class="table table-bordered table-hover w-100" id="tableMerk">
        <thead>
          <tr>
            <th>No</th>
            <th>Merk</th>
            <th data-orderable="false">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; ?>
          <?php foreach ($list_merk as $merk) : ?>
            <tr>
              <td><?= $no++ ?></td>
              <td><?= $merk->merk ?></td>
              <td>
                <?php if ($allowed_edit) : ?>
                  <button class="btn btn-primary btn-sm" onclick="editMerk('<?= encrypt_url($merk->merk_id) ?>', '<?= $merk->merk ?>')">Edit</button>
                <?php endif; ?>
                <?php if ($allowed_delete) : ?>
                  <button class="btn btn-danger btn-sm" onclick="deleteMerk('<?= encrypt_url($merk->merk_id) ?>')">Hapus</button>
                <?php endif; ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php if ($allowed_add) : ?>
  <div class="modal fade" id="modalAddMerkKendaraan">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4>Tambah Merk</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <?= form_open("/merk-kendaraan/create-merk") ?>

          <?php $this->load->view("components/input-group", ["label" => "Merk Kendaraan", "name" => "merk"]); ?>

          <button class="btn btn-success btn-sm mt-2" type="submit">Tambah Merk</button>
          </form>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
<?php endif; ?>

<?php if ($allowed_edit) : ?>
  <div class="modal fade" id="modalEditMerkKendaraan">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4>Edit Merk</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <?= form_open("/merk-kendaraan/edit-merk") ?>
          <input type="hidden" name="merk_id" id="inputHiddenMerkID">

          <?php $this->load->view("components/input-group", ["label" => "Merk Kendaraan", "name" => "merk", "id_custom" => "id='inputEditMerk'"]); ?>

          <button class="btn btn-success btn-sm mt-2" type="submit">Edit Merk</button>
          </form>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
<?php endif; ?>

<?php if ($allowed_delete) : ?>
  <div class="modal fade" id="modalDeleteMerkKendaraan">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal body -->
        <div class="modal-body">
          <h4>Hapus Merk Ini ?</h4>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <a href="<?= base_url("merk-kendaraan") ?>" id="btnDeleteMerk" class="btn btn-danger btn-sm">Hapus Merk</a>
          <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
<?php endif; ?>

<script>
  let tableMerk = dataTable("tableMerk");

  function editMerk(merkID, merk) {
    $("#inputHiddenMerkID").val(merkID);
    $("#inputEditMerk").val(merk);
    $("#modalEditMerkKendaraan").modal("show");
  }

  function deleteMerk(merkID) {
    document.querySelector("#btnDeleteMerk").href = `<?= base_url("merk-kendaraan/delete-merk") ?>/${merkID}`;

    $("#modalDeleteMerkKendaraan").modal("show");
  }
</script>