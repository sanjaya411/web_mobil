<div class="container-fluid dashboard">
  <?= $breadcrumb ?>
  <h4>UAC Management</h4>
  <nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
      <a class="nav-item nav-link active" data-toggle="tab" href="#navAccessControl" role="tab">Position</a>
      <a class="nav-item nav-link" data-toggle="tab" href="#navDivision" role="tab">Division</a>
    </div>
  </nav>
  <div class="card">
    <div class="card-body">
      <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="navAccessControl" role="tabpanel">
          <?php if ($allow_add_position) : ?>
            <button class="btn btn-success btn-sm mb-3" data-toggle="modal" data-target="#modalAddPosition">Tambah Posisi</button>
          <?php endif; ?>
          <table class="table table-bordered table-hover w-100">
            <thead>
              <tr>
                <th>Position</th>
                <th>Division</th>
                <th>Total Admin</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php if (is_array($list_access_control) && count($list_access_control) > 0) : ?>
                <?php foreach ($list_access_control as $uac) : ?>
                  <tr>
                    <td><?= $uac->access_levelName ?></td>
                    <td><?= $uac->division_name ?></td>
                    <td><?= $uac->admin_count ?></td>
                    <td>
                      <a href="<?= base_url("uac-management/") . encrypt_url($uac->admin_tier_id) ?>" class="btn btn-success btn-sm">Detail</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              <?php else : ?>
                <tr>
                  <td colspan="4" class="text-center">No Data Admin</td>
                </tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
        <div class="tab-pane fade" id="navDivision" role="tabpanel">
          <?php if ($allow_add_division) : ?>
            <button class="btn btn-success btn-sm mb-3" data-toggle="modal" data-target="#modalAddDivision">Add Division</button>
          <?php endif; ?>
          <table class="table table-bordered table-hover w-100">
            <thead>
              <tr>
                <th>No</th>
                <th>Division</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1; ?>
              <?php foreach ($list_division as $division) : ?>
                <tr>
                  <td><?= $no++; ?></td>
                  <td><?= $division->division_name ?></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>


<?php if ($allow_add_division) : ?>
  <div class="modal fade" id="modalAddDivision">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <?= form_open("/uac/create-division") ?>
          <div class="form-group">
            <label>Division</label>
            <input type="text" class="form-control" name="division_name">
          </div>
          <button class="btn btn-success btn-sm mt-2" type="submit">Create New Division</button>
          </form>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
<?php endif; ?>

<?php if ($allow_add_position) : ?>

  <div class="modal fade" id="modalAddPosition">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <?= form_open("/uac/create-position") ?>
          <div class="form-group">
            <label>Position</label>
            <input type="text" class="form-control" name="access_levelName">
          </div>
          <div class="form-group">
            <label>Divisi</label>
            <select name="access_divisionId" class="form-control">
              <?php foreach ($list_division as $division) : ?>
                <?php if ($division->division_id > 1) : ?>
                  <option value="<?= $division->division_id ?>"><?= $division->division_name ?></option>
                <?php endif; ?>
              <?php endforeach; ?>
            </select>
          </div>

          <button class="btn btn-success btn-sm mt-2" type="submit">Create New Position</button>
          </form>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>

<?php endif; ?>