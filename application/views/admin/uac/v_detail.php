<div class="container-fluid dashboard">
  <?= $breadcrumb ?>

  <div class="card">
    <div class="card-body">
      <table class="table">
        <tr>
          <td>Position Name</td>
          <td>: <?= $position->access_levelName ?></td>
        </tr>
        <tr>
          <td>Division</td>
          <td>: <?= $position->division_name ?></td>
        </tr>
      </table>
    </div>
  </div>
</div>

<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" data-toggle="tab" href="#navListAdmin" role="tab">Admin List</a>
    <?php if ($allow_change_access) : ?>
      <a class="nav-item nav-link" data-toggle="tab" href="#navModule" role="tab">Access List</a>
    <?php endif; ?>
  </div>
</nav>

<div class="card">
  <div class="card-body">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="navListAdmin" role="tabpanel">
        <table class="table table-bordered table-hover w-100">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php if (is_array($list_admin) && count($list_admin) > 0) : ?>
              <?php foreach ($list_admin as $admin) : ?>
                <tr>
                  <td><?= $admin->admin_name ?></td>
                  <td><?= $admin->admin_email ?></td>
                  <td><?= $admin->admin_phone ?></td>
                  <td>
                    <a href="<?= base_url("admin_detail?id=") . encrypt_url($admin->admin_id) ?>" class="btn btn-success btn-sm">Detail</a>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else : ?>
              <tr>
                <td colspan="4" class="text-center">No Data Admin</td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>

      <?php if ($allow_change_access) : ?>
        <div class="tab-pane fade" id="navModule" role="tabpanel">
          <?= form_open("uac/" . encrypt_url($position->admin_tier_id) . "/assign") ?>
          <input type="hidden" name="admin_tier_id" value="<?= encrypt_url($position->admin_tier_id) ?>">
          <table class="table table-bordered table-hover w-100">
            <thead>
              <tr>
                <th>Module Name</th>
                <th>Status Allowed</th>
                <th>Authorized By</th>
                <th>Authorized Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php if ($position->access_divisionId == 1) : ?>
                <tr>
                  <td colspan="5" class="text-center">Super Admin Is Allowed All Module</td>
                </tr>
              <?php else : ?>
                <?php foreach ($list_access as $access) : ?>
                  <tr>
                    <td><?= $access->action ?></td>
                    <td><?= !empty($access->assign_date) ? "Yes" : "No" ?></td>
                    <td><?= !empty($access->admin_name) ? $access->admin_name : "-" ?></td>
                    <td><?= !empty($access->assign_date) ? date("d M Y H:i", strtotime($access->assign_date)) : "-" ?></td>
                    <td>
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="action_id[]" value="<?= encrypt_url($access->action_id) ?>" class="custom-control-input" id="customCheck<?= $access->action_id ?>" <?= !empty($access->assign_date) ? "checked" : "" ?>>
                        <label class="custom-control-label" for="customCheck<?= $access->action_id ?>">Assign Module</label>
                      </div>
                    </td>
                  </tr>
                <?php endforeach; ?>
              <?php endif; ?>
            </tbody>
          </table>

          <button class="btn btn-success btn-sm mt-3" type="submit">Update Access</button>
          <?= form_close() ?>
        </div>
      <?php endif; ?>
      
    </div>
  </div>
</div>