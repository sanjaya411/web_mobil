<div class="container-fluid dashboard">
  <?= $breadcrumb ?>
  <h4>Service</h4>

  <div class="card">
    <div class="card-body">
      <?php if ($allowed_add) : ?>
        <button class="btn btn-success btn-sm mb-3" data-toggle="modal" data-target="#modalAddService">Tambah Service</button>
      <?php endif; ?>

      <table class="table table-bordered table-hover w-100" id="tableService">
        <thead>
          <tr>
            <th>No</th>
            <th>Service</th>
            <th data-orderable="false">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; ?>
          <?php foreach ($list_service as $service) : ?>
            <tr>
              <td><?= $no++ ?></td>
              <td><?= $service->service ?></td>
              <td>
                <?php if ($allowed_edit) : ?>
                  <button class="btn btn-primary btn-sm" onclick="editService('<?= encrypt_url($service->service_id) ?>', '<?= $service->service ?>')">Edit</button>
                <?php endif; ?>
                <?php if ($allowed_delete) : ?>
                  <button class="btn btn-danger btn-sm" onclick="deleteService('<?= encrypt_url($service->service_id) ?>')">Hapus</button>
                <?php endif; ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php if ($allowed_add) : ?>
  <div class="modal fade" id="modalAddService">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4>Tambah Service</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <?= form_open("/service/create-service") ?>

          <?php $this->load->view("components/input-group", ["label" => "Service Kendaraan", "name" => "service"]); ?>

          <button class="btn btn-success btn-sm mt-2" type="submit">Tambah Service</button>
          </form>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
<?php endif; ?>

<?php if ($allowed_edit) : ?>
  <div class="modal fade" id="modalEditService">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4>Edit Service</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <?= form_open("/service/edit-service") ?>
          <input type="hidden" name="service_id" id="inputHiddenServiceID">

          <?php $this->load->view("components/input-group", ["label" => "Service Kendaraan", "name" => "service", "id_custom" => "id='inputEditService'"]); ?>

          <button class="btn btn-success btn-sm mt-2" type="submit">Edit Service</button>
          </form>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
<?php endif; ?>

<?php if ($allowed_delete) : ?>
  <div class="modal fade" id="modalDeleteService">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal body -->
        <div class="modal-body">
          <h4>Hapus service Ini ?</h4>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <a href="<?= base_url("service") ?>" id="btnDeleteService" class="btn btn-danger btn-sm">Hapus service</a>
          <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
<?php endif; ?>

<script>
  let tableService = dataTable("tableService");

  function editService(serviceID, service) {
    $("#inputHiddenServiceID").val(serviceID);
    $("#inputEditService").val(service);
    $("#modalEditService").modal("show");
  }

  function deleteService(serviceID) {
    document.querySelector("#btnDeleteService").href = `<?= base_url("service/delete-service") ?>/${serviceID}`;

    $("#modalDeleteService").modal("show");
  }
</script>