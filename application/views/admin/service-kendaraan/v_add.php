<div class="container-fluid dashboard">
  <?= $breadcrumb ?>

  <div class="card">
    <div class="card-body">
      <?= form_open_multipart("service-kendaraan/create-service-kendaraan") ?>

        <?php $this->load->view("admin/service-kendaraan/v_form", ["list_kendaraan" => $list_kendaraan, "list_service" => $list_service]); ?>

        <button class="btn btn-success btn-sm" type="submit">Tambah</button>
      </form>
    </div>
  </div>
</div>