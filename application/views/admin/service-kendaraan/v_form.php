<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>Mobil</label>
      <select name="kendaraan_id" class="form-control" required>
        <?php foreach ($list_kendaraan as $kendaraan) : ?>
          <?php if (isset($service_kendaraan->kendaraan_id)) : ?>
            <option <?php if ($service_kendaraan->kendaraan_id == $service_kendaraan->kendaraan_id) echo "selected"; ?> value="<?= encrypt_url($kendaraan->kendaraan_id) ?>"><?= "$kendaraan->nama - $kendaraan->no_polisi" ?></option>
          <?php else : ?>
            <option value="<?= encrypt_url($kendaraan->kendaraan_id) ?>"><?= "$kendaraan->nama - $kendaraan->no_polisi" ?></option>
          <?php endif; ?>
        <?php endforeach; ?>
      </select>
    </div>

    <div class="form-group">
      <label>Harga Service</label>
      <input type="number" name="harga" class="form-control" value="<?= $service_kendaraan->harga ?? "" ?>">
    </div>

    <div class="form-group">
      <label>Tanggal Service</label>
      <input type="date" name="service_date" class="form-control" value="<?= $service_kendaraan->service_date ?? date("Y-m-d") ?>">
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>Service</label>
      <select name="service_id" class="form-control" required>
        <?php foreach ($list_service as $service) : ?>
          <?php if (isset($service_kendaraan->service_id)) : ?>
            <option <?php if ($service_kendaraan->service_id == $service->service_id) echo "selected"; ?> value="<?= encrypt_url($service->service_id) ?>"><?= $service->service ?></option>
          <?php else : ?>
            <option value="<?= encrypt_url($service->service_id) ?>"><?= $service->service ?></option>
          <?php endif; ?>
        <?php endforeach; ?>
      </select>
    </div>

    <div class="form-group">
      <label>Struk</label>
      <input type="file" class="form-control" name="struk">
    </div>
  </div>
</div>