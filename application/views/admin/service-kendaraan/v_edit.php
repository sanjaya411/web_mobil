<div class="container-fluid dashboard">
  <?= $breadcrumb ?>

  <div class="card">
    <div class="card-body">
      <?= form_open_multipart("service-kendaraan/edit-service-kendaraan") ?>
        <input type="hidden" name="rel_id" value="<?= encrypt_url($service_kendaraan->rel_id) ?>">
        <?php $this->load->view("admin/service-kendaraan/v_form", ["list_kendaraan" => $list_kendaraan, "list_service" => $list_service, "service_kendaraan" => $service_kendaraan]); ?>

        <button class="btn btn-success btn-sm" type="submit">Edit</button>
      </form>
    </div>
  </div>
</div>