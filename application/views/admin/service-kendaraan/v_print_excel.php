<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Laporan Data Service Kendaraan</title>
  <style>
    h1 {
      font-family: sans-serif;
    }

    table {
      font-family: Arial, Helvetica, sans-serif;
      color: #000000;
      font-size: 50%;
      text-shadow: 1px 1px 0px #fff;
      /* background: #eaebec; */
      border: #000000 1px solid;
    }

    table th {
      padding: 6px 6px;
      border-left: 1px solid #000000;
      border-bottom: 1px solid #000000;
      /* background: #ededed; */
    }

    table th:first-child {
      border-left: none;
    }

    table tr {
      text-align: center
    }

    table td:first-child {
      text-align: left;
      padding-left: 5px;
      border-left: 0;
    }

    table td {
      padding: 0px 0px;
      border-top: 1px solid #000000;
      border-bottom: 1px solid #000000;
      border-left: 1px solid #000000;
      /* background: #fafafa; */
      background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
      background: -moz-linear-gradient(top, #fbfbfb, #fafafa);
    }

    table tr:last-child td {
      border-bottom: 0;
    }

    table tr:last-child td:first-child {
      -moz-border-radius-bottomleft: 3px;
      -webkit-border-bottom-left-radius: 3px;
      border-bottom-left-radius: 3px;
    }

    table tr:last-child td:last-child {
      -moz-border-radius-bottomright: 3px;
      -webkit-border-bottom-right-radius: 3px;
      border-bottom-right-radius: 3px;
    }

    table tr:hover td {
      /* background: #f2f2f2; */
      background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#f0f0f0));
      background: -moz-linear-gradient(top, #f2f2f2, #f0f0f0);
    }
  </style>
</head>

<body>

  <center>
    <h4>Laporan Data Kendaraan</h4>
    <hr>
  </center>
  <?php
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Laporan Data Service Kendaraan.xls"); ?>

  <table cellspacing='0'>
    <thead style="background-color:cornflowerblue; color:#f2f2f2;">
    <tr>
        <th>No</th>
        <th>Mobil</th>
        <th>Merk Mobil</th>
        <th>Jenis Mobil</th>
        <th>No Polisi</th>
        <th>Service</th>
        <th>Tanggal Service</th>
        <th>Struk</th>
      </tr>
    </thead>
    <tbody>
    <?php $no = 1; ?>
      <?php foreach ($list_service as $service) : ?>
        <tr>
          <td><?= $no++ ?></td>
          <td><?= $service->nama ?></td>
          <td><?= $service->merk ?></td>
          <td><?= $service->jenis ?></td>
          <td><?= $service->no_polisi ?></td>
          <td><?= $service->service ?></td>
          <td><?= date("d M Y", strtotime($service->service_date)) ?></td>
          <td>
            <img src="<?= base_url("assets/image/struk-service/$service->file") ?>" alt="Struk Image" class="img-fluid" style="max-height: 10px;">
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

</body>

</html>