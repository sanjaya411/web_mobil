<div class="container-fluid dashboard">
  <?= $breadcrumb ?>
  <h4>Service Kendaraan</h4>

  <div class="card mb-3">
    <div class="card-body">
      <form action="">
        <div class="row">
          <div class="col-md-4">
            <input type="date" name="start_date" class="form-control input-filter" value="<?= $start_date ?>">
          </div>
          <div class="col-md-4">
            <input type="date" name="end_date" class="form-control input-filter" value="<?= $end_date ?>">
          </div>
          <div class="col-md-4">
            <button class="btn btn-info btn-sm">Filter</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <div class="card">
    <div class="card-body">
      <div class="mb-3">
        <?php if ($allowed_add) : ?>
          <a href="<?= base_url("service-kendaraan/create") ?>" class="btn btn-success btn-sm">Tambah Baru</a>
        <?php endif; ?>
        <button class="btn btn-primary btn-sm" onclick="generatePDF()">Cetak PDF</button>
        <button class="btn btn-primary btn-sm" onclick="generateExcel()">Cetak Excel</button>
      </div>

      <table class="table table-bordered table-hover" id="tableServiceKendaraan">
        <thead>
          <tr>
            <th>No</th>
            <th>Mobil</th>
            <th>Merk Mobil</th>
            <th>Jenis Mobil</th>
            <th>No Polisi</th>
            <th>Service</th>
            <th>Tanggal Service</th>
            <th data-orderable="false">Struk</th>
            <th data-orderable="false">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; ?>
          <?php foreach ($list_service as $service) : ?>
            <tr>
              <td><?= $no++ ?></td>
              <td><?= $service->nama ?></td>
              <td><?= $service->merk ?></td>
              <td><?= $service->jenis ?></td>
              <td><?= $service->no_polisi ?></td>
              <td><?= $service->service ?></td>
              <td><?= date("d M Y", strtotime($service->service_date)) ?></td>
              <td>
                <img src="<?= base_url("assets/image/struk-service/$service->file") ?>" alt="Struk Image" class="img-fluid" style="max-height: 200px;">
              </td>
              <td>
                <?php if ($allowed_add) : ?>
                  <a href="<?= base_url("service-kendaraan/edit/" . encrypt_url($service->rel_id)) ?>" class="btn btn-primary btn-sm">Edit</a>
                <?php endif; ?>
                <?php if ($allowed_add) : ?>
                  <button onclick="modalDelete('<?= base_url('service-kendaraan/delete/' . encrypt_url($service->rel_id)) ?>')" class="btn btn-danger btn-sm">Hapus</button>
                <?php endif; ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php if ($allowed_delete) : ?>
  <div class="modal fade" id="modalDeleteService">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal body -->
        <div class="modal-body">
          <h4>Hapus service Ini ?</h4>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <a href="<?= base_url("service") ?>" id="btnDeleteService" class="btn btn-danger btn-sm">Hapus service</a>
          <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
<?php endif; ?>

<script>
  let tableServiceKendaraan = dataTable("tableServiceKendaraan");

  function generatePDF() {
    let queryString = "";
    let delimiter = "";
    let listInput = document.querySelectorAll(".input-filter");

    listInput.forEach(input => {
      queryString += `${delimiter}${input.name}=${input.value}`;
      delimiter = "&";
    });

    // console.log(queryString);
    let fullUrl = `<?= site_url("service-kendaraan/print-pdf") ?>?${queryString}`;
    window.open(fullUrl, "_blank");
  }

  function generateExcel() {
    let queryString = "";
    let delimiter = "";
    let listInput = document.querySelectorAll(".input-filter");

    listInput.forEach(input => {
      queryString += `${delimiter}${input.name}=${input.value}`;
      delimiter = "&";
    });

    // console.log(queryString);
    let fullUrl = `<?= site_url("service-kendaraan/print-excel") ?>?${queryString}`;
    window.open(fullUrl, "_blank");
  }

  function modalDelete(url) {
    $("#btnDeleteService").attr("href", url);
    $("#modalDeleteService").modal("show");
  }
</script>