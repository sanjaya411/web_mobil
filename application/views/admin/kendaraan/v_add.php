<div class="container-fluid dashboard">
  <?= $breadcrumb ?>

  <div class="card">
    <div class="card-body">
      <?= form_open("kendaraan/create-kendaraan") ?>

        <?php $this->load->view("admin/kendaraan/v_form", ["list_merk" => $list_merk, "list_jenis" => $list_jenis]); ?>

        <button class="btn btn-success btn-sm" type="submit">Tambah Kendaraan</button>
      </form>
    </div>
  </div>
</div>