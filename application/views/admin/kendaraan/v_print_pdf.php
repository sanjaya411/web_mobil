<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?= $title_pdf ?></title>
  <style>
		h1 {
			font-family: sans-serif;
		}

		table {
			font-family: Arial, Helvetica, sans-serif;
			color: #666;
			font-size: 50%;
			text-shadow: 1px 1px 0px #fff;
			/* background: #eaebec; */
			border: #ccc 1px solid;
		}

		table th {
			padding: 6px 6px;
			border-left: 1px solid #e0e0e0;
			border-bottom: 1px solid #e0e0e0;
			/* background: #ededed; */
		}

		table th:first-child {
			border-left: none;
		}

		table tr {
			text-align: center
		}

		table td:first-child {
			text-align: left;
			padding-left: 5px;
			border-left: 0;
		}

		table td {
			padding: 0px 0px;
			border-top: 1px solid #ffffff;
			border-bottom: 1px solid #e0e0e0;
			border-left: 1px solid #e0e0e0;
			/* background: #fafafa; */
			background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
			background: -moz-linear-gradient(top, #fbfbfb, #fafafa);
		}

		table tr:last-child td {
			border-bottom: 0;
		}

		table tr:last-child td:first-child {
			-moz-border-radius-bottomleft: 3px;
			-webkit-border-bottom-left-radius: 3px;
			border-bottom-left-radius: 3px;
		}

		table tr:last-child td:last-child {
			-moz-border-radius-bottomright: 3px;
			-webkit-border-bottom-right-radius: 3px;
			border-bottom-right-radius: 3px;
		}

		table tr:hover td {
			/* background: #f2f2f2; */
			background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#f0f0f0));
			background: -moz-linear-gradient(top, #f2f2f2, #f0f0f0);
		}
	</style>
</head>

<body>

<center>
		<h4>Laporan Data Surat Kendaraan</h4>
		<hr>
	</center>
	<?php date_default_timezone_set('Asia/Jakarta');
	echo date('l, d-m-Y '); ?> /<?php date_default_timezone_set('Asia/Jakarta');
								echo date('H:i:s a'); ?>

  <table cellspacing='0'>
    <thead style="background-color:cornflowerblue; color:#f2f2f2;">
      <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Merk</th>
        <th>Jenis</th>
        <th>Warna</th>
        <th>Tahun Kendaraan</th>
        <th>Tanggal Beli</th>
        <th>No. Polisi</th>
        <th>Tanggal Pajak</th>
        <th>STNK</th>
      </tr>
    </thead>
    <tbody>
      <?php $no = 1; ?>
      <?php foreach ($list_kendaraan as $kendaraan) : ?>
        <tr>
          <td><?= $no++; ?></td>
          <td><?= $kendaraan->nama; ?></td>
          <td><?= $kendaraan->merk; ?></td>
          <td><?= $kendaraan->jenis; ?></td>
          <td><?= $kendaraan->warna; ?></td>
          <td><?= $kendaraan->thn_kendaraan; ?></td>
          <td><?= date("d M Y", strtotime($kendaraan->tgl_beli)); ?></td>
          <td><?= $kendaraan->no_polisi; ?></td>
          <td><?= date("d M Y", strtotime($kendaraan->tgl_pajak)); ?></td>
          <td><?= date("d M Y", strtotime($kendaraan->stnk)); ?></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

</body>

</html>