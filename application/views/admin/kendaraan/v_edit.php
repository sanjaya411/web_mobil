<div class="container-fluid dashboard">
  <?= $breadcrumb ?>

  <div class="card">
    <div class="card-body">
      <?= form_open("kendaraan/edit-kendaraan") ?>

        <input type="hidden" name="kendaraan_id" value="<?= encrypt_url($kendaraan->kendaraan_id) ?>" >
        <?php $this->load->view("admin/kendaraan/v_form", ["list_merk" => $list_merk, "list_jenis" => $list_jenis, "kendaraan" => $kendaraan]); ?>

        <button class="btn btn-success btn-sm" type="submit">Edit Kendaraan</button>
      </form>
    </div>
  </div>
</div>