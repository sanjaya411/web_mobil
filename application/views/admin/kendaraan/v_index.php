<div class="container-fluid dashboard">
  <?= $breadcrumb ?>
  <h4>Kendaraan</h4>

  <div class="card mb-3">
    <div class="card-body">
      <form action="">
        <div class="row">
          <div class="col-md-4">
            <input type="date" name="start_date" class="form-control input-filter" value="<?= $start_date ?>">
          </div>
          <div class="col-md-4">
            <input type="date" name="end_date" class="form-control input-filter" value="<?= $end_date ?>">
          </div>
          <div class="col-md-4">
            <button class="btn btn-info btn-sm">Filter</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <div class="card">
    <div class="card-body">
      <div class="mb-3">
        <?php if ($allowed_add) : ?>
          <a href="<?= base_url("kendaraan/create") ?>" class="btn btn-success btn-sm">Tambah Kendaraan</a>
        <?php endif; ?>
        <button class="btn btn-primary btn-sm" onclick="generatePDF()">Cetak PDF</button>
        <button class="btn btn-primary btn-sm" onclick="generateExcel()">Cetak Excel</button>
      </div>

      <table class="table table-bordered table-hover w-100 table-responsive" id="tableKendaraan">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Merk</th>
            <th>Jenis</th>
            <th>Warna</th>
            <th>Tahun Kendaraan</th>
            <th>Tanggal Beli</th>
            <th>No. Polisi</th>
            <th>Tanggal Pajak</th>
            <th>STNK</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; ?>
          <?php foreach ($list_kendaraan as $kendaraan) : ?>
            <tr>
              <td><?= $no++; ?></td>
              <td><?= $kendaraan->nama; ?></td>
              <td><?= $kendaraan->merk; ?></td>
              <td><?= $kendaraan->jenis; ?></td>
              <td><?= $kendaraan->warna; ?></td>
              <td><?= $kendaraan->thn_kendaraan; ?></td>
              <td><?= date("d M Y", strtotime($kendaraan->tgl_beli)); ?></td>
              <td><?= $kendaraan->no_polisi; ?></td>
              <td><?= date("d M Y", strtotime($kendaraan->tgl_pajak)); ?></td>
              <td><?= date("d M Y", strtotime($kendaraan->stnk)); ?></td>
              <td>
                <?php if ($allowed_edit) : ?>
                  <a href="<?= base_url("kendaraan/edit/" . encrypt_url($kendaraan->kendaraan_id)); ?>" class="btn btn-primary btn-sm">Edit</a>
                <?php endif; ?>
                <?php if ($allowed_deleted) : ?>
                  <button class="btn btn-danger btn-sm" onclick="deleteKendaraan('<?= base_url('kendaraan/delete/' . encrypt_url($kendaraan->kendaraan_id)) ?>')">Delete</button>
                <?php endif; ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php if ($allowed_deleted) : ?>
  <div class="modal fade" id="modalDeleteKendaraan">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal body -->
        <div class="modal-body">
          <h4>Hapus kendaraan Ini ?</h4>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <a href="<?= base_url("kendaraan") ?>" id="btnDeleteKendaraan" class="btn btn-danger btn-sm">Hapus kendaraan</a>
          <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
<?php endif; ?>

<script>
  let tableKendaraan = dataTable("tableKendaraan");

  function generatePDF() {
    let queryString = "";
    let delimiter = "";
    let listInput = document.querySelectorAll(".input-filter");

    listInput.forEach(input => {
        queryString += `${delimiter}${input.name}=${input.value}`;
        delimiter = "&";
    });

    // console.log(queryString);
    let fullUrl = `<?= site_url("kendaraan/print-pdf") ?>?${queryString}`;
    window.open(fullUrl, "_blank");
  }

  function generateExcel() {
    let queryString = "";
    let delimiter = "";
    let listInput = document.querySelectorAll(".input-filter");

    listInput.forEach(input => {
        queryString += `${delimiter}${input.name}=${input.value}`;
        delimiter = "&";
    });

    // console.log(queryString);
    let fullUrl = `<?= site_url("kendaraan/print-excel") ?>?${queryString}`;
    window.open(fullUrl, "_blank");
  }

  function deleteKendaraan(fulUrl) {
    document.querySelector("#btnDeleteKendaraan").href = fulUrl;

    $("#modalDeleteKendaraan").modal("show");
  }
</script>