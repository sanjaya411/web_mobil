<div class="row">
  <div class="col-md-6">

    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" name="nama" value="<?= $kendaraan->nama ?? ""; ?>">
    </div>

    <div class="form-group">
      <label>Merk</label>
      <select name="merk_id" class="form-control">
        <?php foreach ($list_merk as $merk) : ?>
          <?php if (isset($kendaraan->merk_id)) : ?>
            <option <?php if ($kendaraan->merk_id == $merk->merk_id) echo "selected"; ?> value="<?= encrypt_url($merk->merk_id) ?>"><?= $merk->merk ?></option>
          <?php else : ?>
            <option value="<?= encrypt_url($merk->merk_id) ?>"><?= $merk->merk ?></option>
          <?php endif; ?>
        <?php endforeach; ?>
      </select>
    </div>

    <div class="form-group">
      <label>Tahun kendaraan</label>
      <input type="number" class="form-control" name="thn_kendaraan" value="<?= $kendaraan->thn_kendaraan ?? ""; ?>">
    </div>

    <div class="form-group">
      <label>No. Polisi</label>
      <input type="text" class="form-control" name="no_polisi" value="<?= $kendaraan->no_polisi ?? ""; ?>">
    </div>

    <div class="form-group">
      <label>STNK</label>
      <input type="date" class="form-control" name="stnk" value="<?= $kendaraan->stnk ?? ""; ?>">
    </div>

    <div class="form-group">
      <label>Kilometer/KM</label>
      <input type="number" class="form-control" name="kilometer" value="<?= $kendaraan->kilometer ?? ""; ?>">
    </div>

  </div>
  <div class="col-md-6">

    <div class="form-group">
      <label>Jenis</label>
      <select name="jenis_id" class="form-control">
        <?php foreach ($list_jenis as $jenis) : ?>
          <?php if (isset($kendaraan->jenis_id)) : ?>
            <option <?php if ($kendaraan->jenis_id == $jenis->jenis_id) echo "selected"; ?> value="<?= encrypt_url($jenis->jenis_id) ?>"><?= $jenis->jenis ?></option>
          <?php else : ?>
            <option value="<?= encrypt_url($jenis->jenis_id) ?>"><?= $jenis->jenis ?></option>
          <?php endif; ?>
        <?php endforeach; ?>
      </select>
    </div>

    <div class="form-group">
      <label>Warna Kendaraan</label>
      <input type="text" class="form-control" name="warna" value="<?= $kendaraan->warna ?? ""; ?>">
    </div>

    <div class="form-group">
      <label>Tanggal Beli</label>
      <input type="date" class="form-control" name="tgl_beli" value="<?= $kendaraan->tgl_beli ?? ""; ?>">
    </div>

    <div class="form-group">
      <label>Tanggal Pajak</label>
      <input type="date" class="form-control" name="tgl_pajak" value="<?= $kendaraan->tgl_pajak ?? ""; ?>">
    </div>

    <div class="form-group">
      <label>PIC</label>
      <input type="text" class="form-control" name="pic" value="<?= $kendaraan->pic ?? ""; ?>">
    </div>

  </div>
</div>