	<!--Sidebar-->
	<div class="sidebar transition overlay-scrollbars">
		<div class="logo d-block py-3 text-center">
			<h3 style="font-weight: 700;" class="mb-0">Administrator</h3>
			<p>Halo, <?= $this->session->admin_name; ?></p>
		</div>
		<div class="sidebar-items" style="margin-top: -20px;">
			<hr style="border: 1px solid white;">
			<div class="accordion" id="sidebar-items">
				<ul>

					<p class="menu" style="margin-top: -10px;">Apps</p>

					<li>
						<a href="<?= base_url("dashboard"); ?>" class="items <?= $title == "Dashboard" ? "active" : FALSE; ?>">
							<i class="fa fa-tachometer-alt"></i>
							<span>Dashoard</span>
						</a>
					</li>
					<?php if (isset($sidebar["allowed_admin_management"]) && $sidebar["allowed_admin_management"] === TRUE) : ?>
						<li>
							<a href="<?= base_url("admin_management") ?>" class="items <?= $title == "Admin Management" ? "active" : FALSE; ?>">
								<i class="fa fa-users"></i>
								<span>Admin Management</span>
							</a>
						</li>
					<?php endif; ?>
					<?php if (isset($sidebar["allowed_jenis_management"]) && $sidebar["allowed_jenis_management"] === TRUE) : ?>
						<li>
							<a href="<?= base_url("jenis-kendaraan") ?>" class="items <?= $title == "Jenis Kendaraan" ? "active" : FALSE; ?>">
								<i class="fa fa-car"></i>
								<span>Jenis Kendaraan</span>
							</a>
						</li>
					<?php endif; ?>
					<?php if (isset($sidebar["allowed_merk_management"]) && $sidebar["allowed_merk_management"] === TRUE) : ?>
						<li>
							<a href="<?= base_url("merk-kendaraan") ?>" class="items <?= $title == "Merk Kendaraan" ? "active" : FALSE; ?>">
								<i class="fa fa-car"></i>
								<span>Merk Kendaraan</span>
							</a>
						</li>
					<?php endif; ?>
					<?php if (isset($sidebar["allowed_service_management"]) && $sidebar["allowed_service_management"] === TRUE) : ?>
						<li>
							<a href="<?= base_url("service") ?>" class="items <?= $title == "Service Management" ? "active" : FALSE; ?>">
								<i class="fa fa-car"></i>
								<span>Service</span>
							</a>
						</li>
					<?php endif; ?>
					<?php if (isset($sidebar["allowed_kendaraan_management"]) && $sidebar["allowed_kendaraan_management"] === TRUE) : ?>
						<li>
							<a href="<?= base_url("kendaraan") ?>" class="items <?= $title == "Kendaraan" ? "active" : FALSE; ?>">
								<i class="fa fa-car"></i>
								<span>Kendaraan</span>
							</a>
						</li>
					<?php endif; ?>
					<?php if (isset($sidebar["allowed_service_kendaraan_management"]) && $sidebar["allowed_service_kendaraan_management"] === TRUE) : ?>
						<li>
							<a href="<?= base_url("service-kendaraan") ?>" class="items <?= $title == "Service Kendaraan" ? "active" : FALSE; ?>">
								<i class="fa fa-car"></i>
								<span>Service Kendaraan</span>
							</a>
						</li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</div>

	<div class="sidebar-overlay"></div>