<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_Jenis extends CR_Model
{
  /**
   * Property untuk men-set nama table. Ini dibuat jika nama table berubah maka cukup ubah 
   * property ini saja
   */
  private $table = "tbl_jenis";

  /**
   * Property untuk men-set id primary table, ini berguna jika nama kolom ID berubah, 
   * maka cukup ubah dari property ini saja
   * 
   */
  private $primaryKey = "jenis_id";

  /**
   * Method untuk mendapatkan semua data jenis
   * 
   */
  public function get_jenis_list()
  {
    return $this->db->get($this->table)->result();
  }

  /**
   * Method untuk menambah jenis
   * Parameter berupa array associative untuk mengisi sesuai kolom table
   * 
   * @param array $values
   */
  public function add_jenis($values)
  {
    $this->db->insert($this->table, $values);
    $this->log_activity(14);
  }

  /**
   * Method untuk mendapatkan data satu jenis
   * Parameter nya berupa integer id jenis nya
   * 
   * @param int $id
   */
  public function get_jenis_detail($id)
  {
    return $this->db->where($this->primaryKey, $id)->get($this->table)->row();
  }

  /**
   * Method untuk update data jenis
   * Parameter nya ada 2,yaitu id jenis yang akan diupdate serta data jenis yang diupdate
   * 
   * @param int $id
   * @param array $values
   */
  public function update_jenis($id, $values)
  {
    $this->db->update($this->table, $values, [$this->primaryKey => $id]);
    $this->log_activity(15);
  }

  /**
   * Method untuk menghapus data jenis
   * Parameter hanya id saja
   * 
   * @param int $id
   */
  public function delete_jenis($id)
  {
    $this->db->delete($this->table, [$this->primaryKey => $id]);
    $this->log_activity(16);
  }

  /**
   * Count All
   */
  public function get_count_jenis()
  {
    return $this->db->get($this->table)->num_rows();
  }

  /**
   * Get count kendaraan by jenis
   * 
   * @param int $jenis_id
   */
  public function get_count_kendaraan_by_jenis($jenis_id)
  {
    return $this->db->get_where("tbl_kendaraan", ["jenis_id" => $jenis_id])->result();
  }
}