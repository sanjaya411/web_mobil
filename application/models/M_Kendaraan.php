<?php defined('BASEPATH') or exit('No direct script access allowed');

class M_kendaraan extends CR_Model
{
  /**
   * Property untuk men-set nama table. Ini dibuat jika nama table berubah maka cukup ubah 
   * property ini saja
   */
  private $table = "tbl_kendaraan";

  /**
   * Property untuk men-set id primary table, ini berguna jika nama kolom ID berubah, 
   * maka cukup ubah dari property ini saja
   * 
   */
  private $primaryKey = "kendaraan_id";

  /**
   * Method untuk mendapatkan semua data kendaraan
   * 
   */
  public function get_kendaraan_list($start_date = FALSE, $end_date = FALSE)
  {
    $query = $this->db->join("`tbl_merk` AS tm", "tm.`merk_id` = {$this->table}.`merk_id`")
      ->join("`tbl_jenis` AS tj", "tj.`jenis_id` = {$this->table}.`jenis_id`");

    if ($start_date && $end_date) {
      $query->where("{$this->table}.`tgl_pajak` BETWEEN '$start_date' AND '$end_date'");
    }

    return $query->get("{$this->table}")->result();
  }

  /**
   * Method untuk menambah kendaraan
   * Parameter berupa array associative untuk mengisi sesuai kolom table
   * 
   * @param array $values
   */
  public function add_kendaraan($values)
  {
    $this->db->insert($this->table, $values);
    $this->log_activity(26);
  }

  /**
   * Method untuk mendapatkan data satu kendaraan
   * Parameter nya berupa integer id kendaraan nya
   * 
   * @param int $id
   */
  public function get_kendaraan_detail($id)
  {
    return $this->db->join("`tbl_merk` AS tm", "tm.`merk_id` = {$this->table}.`merk_id`")
      ->join("`tbl_jenis` AS tj", "tj.`jenis_id` = {$this->table}.`jenis_id`")
      ->where("{$this->table}.`{$this->primaryKey}`", $id)->get($this->table)->row();
  }

  /**
   * Method untuk update data kendaraan
   * Parameter nya ada 2,yaitu id kendaraan yang akan diupdate serta data kendaraan yang diupdate
   * 
   * @param int $id
   * @param array $values
   */
  public function update_kendaraan($id, $values)
  {
    $this->db->update($this->table, $values, [$this->primaryKey => $id]);
    $this->log_activity(27);
  }

  /**
   * Method untuk menghapus data kendaraan
   * Parameter hanya id saja
   * 
   * @param int $id
   */
  public function delete_kendaraan($id)
  {
    $this->db->delete($this->table, [$this->primaryKey => $id]);
    $this->log_activity("32");
  }

  /**
   * Method untuk mencari mobil by No Polisi
   * Parameter berupa string keyword
   * 
   * @param string $keyword
   */
  public function search_kendaraan_by_keyword($keyword)
  {
    $query_results = $this->db->like("no_polisi", $keyword)->get($this->table)->result();

    if (count($query_results) > 0) {
      return $query_results;
    }
    return FALSE;
  }

  /**
   * Count All
   */
  public function get_count_kendaraan()
  {
    return $this->db->get($this->table)->num_rows();
  }

  /**
   * Get tax expired
   * 
   */
  public function get_tax_expired()
  {
    $expired_date = date("Y-m-d", strtotime("+1 month"));
    $query = $this->db->from("tbl_kendaraan")
      ->where("tgl_pajak <=", $expired_date)
      ->get()
      ->result();
    // var_dump(count($query));
    return $query;
  }

  /**
   * Get count service by kendaraan
   * 
   * @param int $kendaraan_id
   */
  public function get_count_kendaraan_service_by_kendaraan($kendaraan_id)
  {
    return $this->db->get_where("tbl_kendaraan_service", ["kendaraan_id" => $kendaraan_id])->result();
  }
}
