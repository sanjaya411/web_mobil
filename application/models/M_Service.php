<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_Service extends CR_Model
{
  /**
   * Property untuk men-set nama table. Ini dibuat jika nama table berubah maka cukup ubah 
   * property ini saja
   */
  private $table = "tbl_service";

  /**
   * Property untuk men-set id primary table, ini berguna jika nama kolom ID berubah, 
   * maka cukup ubah dari property ini saja
   * 
   */
  private $primaryKey = "service_id";

  /**
   * Method untuk mendapatkan semua data service
   * 
   */
  public function get_service_list()
  {
    return $this->db->get($this->table)->result();
  }

  /**
   * Method untuk menambah service
   * Parameter berupa array associative untuk mengisi sesuai kolom table
   * 
   * @param array $values
   */
  public function add_service($values)
  {
    $this->db->insert($this->table, $values);
    $this->log_activity(22);
  }

  /**
   * Method untuk mendapatkan data satu service
   * Parameter nya berupa integer id service nya
   * 
   * @param int $id
   */
  public function get_service_detail($id)
  {
    return $this->db->where($this->primaryKey, $id)->get($this->table)->row();
  }

  /**
   * Method untuk update data service
   * Parameter nya ada 2,yaitu id service yang akan diupdate serta data service yang diupdate
   * 
   * @param int $id
   * @param array $values
   */
  public function update_service($id, $values)
  {
    $this->db->update($this->table, $values, [$this->primaryKey => $id]);
    $this->log_activity(23);
  }

  /**
   * Method untuk menghapus data service
   * Parameter hanya id saja
   * 
   * @param int $id
   */
  public function delete_service($id)
  {
    $this->db->delete($this->table, [$this->primaryKey => $id]);
    $this->log_activity(24);
  }

  /**
   * Get count kendaraan by service
   * 
   * @param int $service_id
   */
  public function get_count_kendaraan_service_by_service($service_id)
  {
    return $this->db->get_where("tbl_kendaraan_service", ["service_id" => $service_id])->result();
  }
}