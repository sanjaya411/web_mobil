<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_Merk extends CR_Model
{
  /**
   * Property untuk men-set nama table. Ini dibuat jika nama table berubah maka cukup ubah 
   * property ini saja
   */
  private $table = "tbl_merk";

  /**
   * Property untuk men-set id primary table, ini berguna jika nama kolom ID berubah, 
   * maka cukup ubah dari property ini saja
   * 
   */
  private $primaryKey = "merk_id";

  /**
   * Method untuk mendapatkan semua data merk
   * 
   */
  public function get_merk_list()
  {
    return $this->db->get($this->table)->result();
  }

  /**
   * Method untuk menambah merk
   * Parameter berupa array associative untuk mengisi sesuai kolom table
   * 
   * @param array $values
   */
  public function add_merk($values)
  {
    $this->db->insert($this->table, $values);
    $this->log_activity(18);
  }

  /**
   * Method untuk mendapatkan data satu merk
   * Parameter nya berupa integer id merk nya
   * 
   * @param int $id
   */
  public function get_merk_detail($id)
  {
    return $this->db->where($this->primaryKey, $id)->get($this->table)->row();
  }

  /**
   * Method untuk update data merk
   * Parameter nya ada 2,yaitu id merk yang akan diupdate serta data merk yang diupdate
   * 
   * @param int $id
   * @param array $values
   */
  public function update_merk($id, $values)
  {
    $this->db->update($this->table, $values, [$this->primaryKey => $id]);
    $this->log_activity(19);
  }

  /**
   * Method untuk menghapus data merk
   * Parameter hanya id saja
   * 
   * @param int $id
   */
  public function delete_merk($id)
  {
    $this->db->delete($this->table, [$this->primaryKey => $id]);
    $this->log_activity(20);
  }

  /**
   * Count All
   */
  public function get_count_merk()
  {
    return $this->db->get($this->table)->num_rows();
  }

  /**
   * Get count kendaraan by merk
   * 
   * @param int $merk_id
   */
  public function get_count_kendaraan_by_merk($merk_id)
  {
    return $this->db->get_where("tbl_kendaraan", ["merk_id" => $merk_id])->result();
  }
}