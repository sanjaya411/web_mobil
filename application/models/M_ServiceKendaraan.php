<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_ServiceKendaraan extends CI_Model
{
  /**
   * Property untuk men-set nama table. Ini dibuat jika nama table berubah maka cukup ubah 
   * property ini saja
   */
  private $table = "tbl_kendaraan_service";

  /**
   * Property untuk men-set id primary table, ini berguna jika nama kolom ID berubah, 
   * maka cukup ubah dari property ini saja
   * 
   */
  private $primaryKey = "rel_id";

  /**
   * Method untuk mendapatkan semua data service_kendaraan
   * 
   */
  public function get_service_kendaraan_list($start_date = FALSE, $end_date = FALSE)
  {
    $query_result = $this->db
            ->join("`tbl_service` AS ts", "ts.`service_id` = $this->table.`service_id`")
            ->join("`tbl_kendaraan` AS tk", "tk.`kendaraan_id` = $this->table.`kendaraan_id`")
            ->join("`tbl_merk` AS tm", "tm.`merk_id` = tk.`merk_id`")
            ->join("`tbl_jenis` AS tj", "tj.`jenis_id` = tk.`jenis_id`");

    if ($start_date !== FALSE && $end_date !== FALSE) {
      $query_result = $query_result->where("$this->table.`service_date` BETWEEN '$start_date' AND '$end_date'");
    }

    return $query_result->get($this->table)->result();
  }

  /**
   * Method untuk menambah service_kendaraan
   * Parameter berupa array associative untuk mengisi sesuai kolom table
   * 
   * @param array $values
   */
  public function add_service_kendaraan($values)
  {
    return $this->db->insert($this->table, $values);
  }

  /**
   * Method untuk mendapatkan data satu service_kendaraan
   * Parameter nya berupa integer id service_kendaraan nya
   * 
   * @param int $id
   */
  public function get_service_kendaraan_detail($id)
  {
    return $this->db
          ->join("`tbl_service` AS ts", "ts.`service_id` = $this->table.`service_id`")
          ->join("`tbl_kendaraan` AS tk", "tk.`kendaraan_id` = $this->table.`kendaraan_id`")
          ->join("`tbl_merk` AS tm", "tm.`merk_id` = tk.`merk_id`")
          ->join("`tbl_jenis` AS tj", "tj.`jenis_id` = tk.`jenis_id`")
          ->where("$this->table.`$this->primaryKey`", $id)->get($this->table)->row();
  }

  /**
   * Method untuk update data service_kendaraan
   * Parameter nya ada 2,yaitu id service_kendaraan yang akan diupdate serta data service_kendaraan yang diupdate
   * 
   * @param int $id
   * @param array $values
   */
  public function update_service_kendaraan($id, $values)
  {
    return $this->db->update($this->table, $values, [$this->primaryKey => $id]);
  }

  /**
   * Method untuk menghapus data service_kendaraan
   * Parameter hanya id saja
   * 
   * @param int $id
   */
  public function delete_service_kendaraan($id)
  {
    return $this->db->delete($this->table, [$this->primaryKey => $id]);
  }

  /**
   * Method untuk upload file
   * Parameter hanya nama input file nya
   * Return nya akan mengembalikan file name dari file yg diupload
   * Ubah path upload pada array upload path
   * 
   * @param string $input_name
   * @return string 
   */
  public function upload_photo($input_name)
  {
    $this->load->library('upload');
    $name_file = "struk_photo_" . time();
    $ext = pathinfo($_FILES[$input_name]['name'], PATHINFO_EXTENSION);

    $config['upload_path']      = './assets/image/struk-service/';
    $config['allowed_types']    = 'gif|jpg|png|jpeg|bmp';
    $config['max_size']         = 15123;
    $config['max_width']        = 15123;
    $config['max_height']       = 15123;
    $config['file_name']        = $name_file;
    
    $this->upload->initialize($config);

    if ($this->upload->do_upload($input_name)) {
      return "$name_file.$ext";
    } else {
      dd($this->upload->display_errors());
    }

    return FALSE;
  }

  public function add($input)
  {
    $values = [
      "service_date" => $input->service_date,
      "kendaraan_id" => decrypt_url($input->kendaraan_id),
      "service_id" => decrypt_url($input->service_id),
      "harga" => $input->harga
    ];

    if (isset($_FILES["struk"]) && $_FILES["struk"]["name"] !== "") {
      $values["file"] = $this->upload_photo("struk");
    }

    $this->add_service_kendaraan($values);
  }

  public function edit($input)
  {
    $rel_id = decrypt_url($input->rel_id);

    $values = [
      "service_date" => $input->service_date,
      "kendaraan_id" => decrypt_url($input->kendaraan_id),
      "service_id" => decrypt_url($input->service_id),
      "harga" => $input->harga
    ];

    if (isset($_FILES["struk"]) && $_FILES["struk"]["name"] !== "") {
      $detail = $this->get_service_kendaraan_detail($rel_id);

      if ($detail->file !== "" && file_exists("./assets/image/struk-service/$detail->file")) {
        unlink("./assets/image/struk-service/$detail->file");
      }
      $values["file"] = $this->upload_photo("struk");
    }
    // dd($values);

    $this->update_service_kendaraan($rel_id, $values);
  }
}