<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis extends CR_Controller
{
  public function __construct()
  {
    parent::__construct();
    $protect_login = $this->Auth->protect_login();
    $this->load->model("M_Jenis", "model_jenis");
    if ($protect_login->success === FALSE) {
      $this->session->set_flashdata("pesan", "<script>sweet('error', 'Gagal!', '$protect_login->message')</script>");
      redirect("login");
      exit;
    }
  }

  public function view_jenis_management()
  {
    $this->check_access("003J", TRUE);

    $data = [
      "list_jenis" => $this->model_jenis->get_jenis_list(),
      "allowed_add" => $this->check_access("003JA"),
      "allowed_edit" => $this->check_access("003JE"),
      "allowed_delete" => $this->check_access("003JD"),
    ];

    $this->view("admin/jenis/v_index", "Jenis Kendaraan", $data, TRUE);
  }

  public function process_jenis_add()
  {
    $this->check_access("003JA", TRUE);

    $input = (object) html_escape($this->input->post());
    $this->model_jenis->add_jenis(["jenis" => $input->jenis]);

    $this->session->set_flashdata("pesan", "<script>sweet('success', 'Sukses!', 'Sukses menambahkan jenis kendaraan')</script>");
    redirect("jenis-kendaraan");
  }

  public function process_jenis_edit()
  {
    $this->check_access("003JE", TRUE);

    $input = (object) html_escape($this->input->post());
    $this->model_jenis->update_jenis(decrypt_url($input->jenis_id), ["jenis" => $input->jenis]);

    $this->session->set_flashdata("pesan", "<script>sweet('success', 'Sukses!', 'Sukses update jenis kendaraan')</script>");
    redirect("jenis-kendaraan");
  }

  public function process_jenis_delete($jenis_id)
  {
    $this->check_access("003JD", TRUE);

    $id = decrypt_url($jenis_id);
    $get_count = $this->model_jenis->get_count_kendaraan_by_jenis($id);

    if (count($get_count) == 0) {
      $this->session->set_flashdata("pesan", "<script>sweet('success', 'Sukses!', 'Sukses menghapus jenis kendaraan')</script>");
      $this->model_jenis->delete_jenis($id);
    } else {
      $this->session->set_flashdata("pesan", "<script>sweet('error', 'Gagal!', 'Jenis kendaraan ini sudah dipakai kendaraan')</script>");
    }

    redirect("jenis-kendaraan");
  }
}