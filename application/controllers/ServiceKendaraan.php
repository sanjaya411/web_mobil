<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ServiceKendaraan extends CR_Controller
{
  public function __construct()
  {
    parent::__construct();
    $protect_login = $this->Auth->protect_login();
    $this->load->model("M_ServiceKendaraan", "model_service_kendaraan");
    $this->load->model("M_Kendaraan", "model_kendaraan");
    $this->load->model("M_Service", "model_service");
    if ($protect_login->success === FALSE) {
      $this->session->set_flashdata("pesan", "<script>sweet('error', 'Gagal!', '$protect_login->message')</script>");
      redirect("login");
      exit;
    }
  }

  public function view_service_kendaraan_management()
  {
    $start_date = $this->input->get("start_date") != NULL ? $this->input->get("start_date") : FALSE;
    $end_date = $this->input->get("end_date") != NULL ? $this->input->get("end_date") : FALSE;

    $data = [
      "list_service" => $this->model_service_kendaraan->get_service_kendaraan_list($start_date, $end_date),
      "allowed_add" => $this->check_access("007SKA"),
      "allowed_edit" => $this->check_access("007SKE"),
      "allowed_delete" => $this->check_access("007SKD"),
      "start_date" => $start_date,
      "end_date" => $end_date,
    ];

    $this->view("admin/service-kendaraan/v_index", "Service Kendaraan", $data, TRUE);
  }

  public function view_service_kendaraan_add()
  {
    $this->check_access("007SKA", TRUE);

    $data = [
      "list_kendaraan" => $this->model_kendaraan->get_kendaraan_list(),
      "list_service" => $this->model_service->get_service_list()
    ];

    $this->view("admin/service-kendaraan/v_add", "Tambah Service Kendaraan", $data);
  }

  public function validate_service_kendaraan_add()
  {
    $this->check_access("007SKA", TRUE);

    $this->form_validation->set_rules("kendaraan_id", "Mobil", "required");
    $this->form_validation->set_rules("service_id", "Service", "required");
    $this->form_validation->set_rules("service_date", "Date Service", "required");
    $this->form_validation->set_rules("harga", "Harga Service", "required|numeric");

    if ($this->form_validation->run() === FALSE) {
      // dd($this->form_error_message());
      $this->session->set_flashdata("pesan", "<script>sweet('error', 'Gagal!', 'Input yang anda masukan salah!')</script>");
      redirect("service-kendaraan/create/");
    } else {
      $this->process_sevrice_kendaraan_add();
    }
  }

  private function process_sevrice_kendaraan_add()
  {
    $this->check_access("007SKA", TRUE);

    $input = (object) $this->input->post();
    $this->model_service_kendaraan->add($input);

    $this->session->set_flashdata("pesan", "<script>sweet('success', 'Berhasil!', 'Service kendaraan berhasil ditambahkan')</script>");
    redirect("service-kendaraan");
  }

  public function view_service_kendaraan_edit($rel_id)
  {
    $this->check_access("007SKE", TRUE);

    $id = decrypt_url($rel_id);
    $data = [
      "list_kendaraan" => $this->model_kendaraan->get_kendaraan_list(),
      "list_service" => $this->model_service->get_service_list(),
      "service_kendaraan" => $this->model_service_kendaraan->get_service_kendaraan_detail($id)
    ];

    $this->view("admin/service-kendaraan/v_edit", "Edit Service Kendaraan", $data);
  }

  public function validate_service_kendaraan_edit()
  {
    $this->form_validation->set_rules("rel_id", "Mobil", "required");
    $this->form_validation->set_rules("kendaraan_id", "Mobil", "required");
    $this->form_validation->set_rules("service_id", "Service", "required");
    $this->form_validation->set_rules("service_date", "Date Service", "required");
    $this->form_validation->set_rules("harga", "Harga Service", "required|numeric");

    if ($this->form_validation->run() === FALSE) {
      // dd($this->form_error_message());
      $this->session->set_flashdata("pesan", "<script>sweet('error', 'Gagal!', 'Input yang anda masukan salah!')</script>");
      redirect("service-kendaraan/edit/" . $this->input->post("rel_id"));
    } else {
      $this->process_sevrice_kendaraan_edit();
    }
  }

  private function process_sevrice_kendaraan_edit()
  {
    $this->check_access("007SKE", TRUE);

    $input = (object) $this->input->post();
    $this->model_service_kendaraan->edit($input);

    $this->session->set_flashdata("pesan", "<script>sweet('success', 'Berhasil!', 'Service kendaraan berhasil ditambahkan')</script>");
    redirect("service-kendaraan");
  }

  public function view_service_kendaraan_delete($rel_id)
  {
    $this->check_access("007SKD", TRUE);

    $id = decrypt_url($rel_id);
    $detail = $this->model_service_kendaraan->get_service_kendaraan_detail($id);

    if ($detail->file !== "" && file_exists("./assets/image/struk-service/$detail->file")) {
      unlink("./assets/image/struk-service/$detail->file");
    }
    $this->model_service_kendaraan->delete_service_kendaraan($id);

    $this->session->set_flashdata("pesan", "<script>sweet('success', 'Berhasil!', 'Service kendaraan berhasil dihapus')</script>");
    redirect("service-kendaraan");
  }

  public function view_service_kendaraan_print_pdf()
  {
    $start_date = $this->input->get("start_date") != NULL ? $this->input->get("start_date") : FALSE;
    $end_date = $this->input->get("end_date") != NULL ? $this->input->get("end_date") : FALSE;

    $data = [
      "list_service" => $this->model_service_kendaraan->get_service_kendaraan_list($start_date, $end_date),
    ];

    $this->load->library('pdfgenerator');
    // filename dari pdf ketika didownload
    $file_pdf = 'Laporan';
    // setting paper
    $paper = 'A4';
    //orientasi paper potrait / landscape
    $orientation = "landscape";

    $html = $this->load->view("admin/service-kendaraan/v_print_pdf", $data, TRUE);

    $this->pdfgenerator->generate($html, $file_pdf, $paper, $orientation);
  }

  public function view_service_kendaraan_print_excel()
  {
    $start_date = $this->input->get("start_date") != NULL ? $this->input->get("start_date") : FALSE;
    $end_date = $this->input->get("end_date") != NULL ? $this->input->get("end_date") : FALSE;

    $data = [
      "list_service" => $this->model_service_kendaraan->get_service_kendaraan_list($start_date, $end_date),
    ];

    $this->load->view("admin/service-kendaraan/v_print_excel", $data);
  }
}