<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CR_Controller
{
  public function __construct()
  {
    parent::__construct();
    $protect_login = $this->Auth->protect_login();
    $this->load->model("M_Service", "model_service");
    if ($protect_login->success === FALSE) {
      $this->session->set_flashdata("pesan", "<script>sweet('error', 'Gagal!', '$protect_login->message')</script>");
      redirect("login");
      exit;
    }
  }

  public function view_service_management()
  {
    $this->check_access("005S", TRUE);

    $data = [
      "list_service" => $this->model_service->get_service_list(),
      "allowed_add" => $this->check_access("005SA"),
      "allowed_edit" => $this->check_access("005SE"),
      "allowed_delete" => $this->check_access("005SD"),
    ];

    $this->view("admin/service/v_index", "Service Management", $data, TRUE);
  }

  public function process_service_add()
  {
    $this->check_access("005SA", TRUE);

    $input = (object) html_escape($this->input->post());
    $this->model_service->add_service(["service" => $input->service]);

    $this->session->set_flashdata("pesan", "<script>sweet('success', 'Berhasil!', 'Service berhasil ditambahkan')</script>");
    redirect("service");
  }

  public function process_service_edit()
  {
    $this->check_access("005SE", TRUE);

    $input = (object) html_escape($this->input->post());
    $service_id = decrypt_url($input->service_id);
    $this->model_service->update_service($service_id, ["service" => $input->service]);

    $this->session->set_flashdata("pesan", "<script>sweet('success', 'Berhasil!', 'Service berhasil diedit')</script>");
    redirect("service");
  }

  public function process_service_delete($service_id)
  {
    $this->check_access("005SD", TRUE);

    $id = decrypt_url($service_id);
    $get_count = $this->model_service->get_count_kendaraan_service_by_service($id);

    if (count($get_count) == 0) {
      $this->model_service->delete_service($id);
      $this->session->set_flashdata("pesan", "<script>sweet('success', 'Berhasil!', 'Service berhasil dihapus')</script>");
    } else {
      $this->session->set_flashdata("pesan", "<script>sweet('error', 'Gagal!', 'Terdapat kendaraan yang menggunakan service ini')</script>");
    }

    redirect("service");
  }
}