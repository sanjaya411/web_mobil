<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kendaraan extends CR_Controller
{
  public function __construct()
  {
    parent::__construct();
    $protect_login = $this->Auth->protect_login();
    $this->load->model("M_Kendaraan", "model_kendaraan");
    $this->load->model("M_Merk", "model_merk");
    $this->load->model("M_Jenis", "model_jenis");
    if ($protect_login->success === FALSE) {
      $this->session->set_flashdata("pesan", "<script>sweet('error', 'Gagal!', '$protect_login->message')</script>");
      redirect("login");
      exit;
    }
  }

  public function view_kendaraan_management()
  {
    $this->check_access("006K", TRUE);

    $start_date = $this->input->get("start_date") != NULL ? $this->input->get("start_date") : FALSE;
    $end_date = $this->input->get("end_date") != NULL ? $this->input->get("end_date") : FALSE;

    $data = [
      "list_kendaraan" => $this->model_kendaraan->get_kendaraan_list($start_date, $end_date),
      "allowed_add" => $this->check_access("006KA"),
      "allowed_edit" => $this->check_access("006KE"),
      "allowed_deleted" => $this->check_access("006KD"),
      "start_date" => $start_date,
      "end_date" => $end_date
    ];

    $this->view("admin/kendaraan/v_index", "Kendaraan", $data, TRUE);
  }

  public function view_kendaraan_add()
  {
    $this->check_access("006KA", TRUE);

    $data = [
      "list_merk" => $this->model_merk->get_merk_list(),
      "list_jenis" => $this->model_jenis->get_jenis_list(),
    ];

    $this->view("admin/kendaraan/v_add", "Tambah Kendaraan", $data);
  }

  public function validate_kendaraan_add()
  {
    $this->check_access("006KA", TRUE);

    $this->form_validation->set_rules("jenis_id", "Jenis Kendaraan", "required");
    $this->form_validation->set_rules("merk_id", "Merk Kendaraan", "required");
    $this->form_validation->set_rules("nama", "Nama Kendaraan", "required");
    $this->form_validation->set_rules("kilometer", "Kilometer Kendaraan", "required|numeric");
    $this->form_validation->set_rules("warna", "warna Kendaraan", "required");
    $this->form_validation->set_rules("thn_kendaraan", "Tahun Kendaraan", "required");
    $this->form_validation->set_rules("tgl_beli", "Tanggal Beli Kendaraan", "required");
    $this->form_validation->set_rules("no_polisi", "No Polisi Kendaraan", "required");
    $this->form_validation->set_rules("tgl_pajak", "Tanggal Pajak Kendaraan", "required");
    $this->form_validation->set_rules("stnk", "Tanggal STNK Pajak Kendaraan", "required");
    $this->form_validation->set_rules("pic", "PIC Kendaraan", "required");

    if ($this->form_validation->run() === FALSE) {
      $this->session->set_flashdata("pesan", "<script>sweet('error', 'Gagal!', 'Input yang anda masukan tidak lengkap!')</script>");
      redirect("kendaraan/create");
    } else {
      $this->process_kendaraan_add();
    }
  }

  private function process_kendaraan_add()
  {
    $values = (array) html_escape($this->input->post()); 
    $values["merk_id"] = decrypt_url($values["merk_id"]);
    $values["jenis_id"] = decrypt_url($values["jenis_id"]);
    $this->model_kendaraan->add_kendaraan($values);

    $this->session->set_flashdata("pesan", "<script>sweet('success', 'Sukses!', 'Sukses menambahkan jenis kendaraan')</script>");
    redirect("kendaraan");
  }

  public function view_kendaraan_edit($kendaraan_id)
  {
    $this->check_access("006KE", TRUE);

    $id = decrypt_url($kendaraan_id);
    $data = [
      "list_merk" => $this->model_merk->get_merk_list(),
      "list_jenis" => $this->model_jenis->get_jenis_list(),
      "kendaraan" => $this->model_kendaraan->get_kendaraan_detail($id)
    ];

    $this->view("admin/kendaraan/v_edit", "Edit Kendaraan", $data);
  }

  public function validate_kendaraan_edit()
  {
    $this->check_access("006KE", TRUE);

    $this->form_validation->set_rules("kendaraan_id", "Jenis Kendaraan", "required");
    $this->form_validation->set_rules("jenis_id", "Jenis Kendaraan", "required");
    $this->form_validation->set_rules("merk_id", "Merk Kendaraan", "required");
    $this->form_validation->set_rules("nama", "Nama Kendaraan", "required");
    $this->form_validation->set_rules("kilometer", "Kilometer Kendaraan", "required|numeric");
    $this->form_validation->set_rules("warna", "warna Kendaraan", "required");
    $this->form_validation->set_rules("thn_kendaraan", "Tahun Kendaraan", "required");
    $this->form_validation->set_rules("tgl_beli", "Tanggal Beli Kendaraan", "required");
    $this->form_validation->set_rules("no_polisi", "No Polisi Kendaraan", "required");
    $this->form_validation->set_rules("tgl_pajak", "Tanggal Pajak Kendaraan", "required");
    $this->form_validation->set_rules("stnk", "Tanggal STNK Pajak Kendaraan", "required");
    $this->form_validation->set_rules("pic", "PIC Kendaraan", "required");

    if ($this->form_validation->run() === FALSE) {
      $this->session->set_flashdata("pesan", "<script>sweet('error', 'Gagal!', 'Input yang anda masukan salah!')</script>");
      redirect("kendaraan/edit-kendaraan/" . $this->input->post("kendaraan_id"));
    } else {
      $this->process_kendaraan_edit();
    }
  }

  private function process_kendaraan_edit()
  {
    $values = (array) html_escape($this->input->post()); 
    $values["merk_id"] = decrypt_url($values["merk_id"]);
    $values["jenis_id"] = decrypt_url($values["jenis_id"]);

    $id = decrypt_url($values["kendaraan_id"]);
    unset($values["kendaraan_id"]);
    $this->model_kendaraan->update_kendaraan($id, $values);

    $this->session->set_flashdata("pesan", "<script>sweet('success', 'Sukses!', 'Sukses update kendaraan')</script>");
    redirect("kendaraan");
  }

  public function view_kendaraan_print_pdf()
  {
    $start_date = $this->input->get("start_date") != NULL ? $this->input->get("start_date") : FALSE;
    $end_date = $this->input->get("end_date") != NULL ? $this->input->get("end_date") : FALSE;

    $data = [
      "list_kendaraan" => $this->model_kendaraan->get_kendaraan_list($start_date, $end_date),
      "title_pdf" => "Laporan",
    ];

    $this->load->library('pdfgenerator');
    // filename dari pdf ketika didownload
    $file_pdf = 'Laporan';
    // setting paper
    $paper = 'A4';
    //orientasi paper potrait / landscape
    $orientation = "landscape";

    $html = $this->load->view("admin/kendaraan/v_print_pdf", $data, TRUE);

    $this->pdfgenerator->generate($html, $file_pdf, $paper, $orientation);
  }

  public function view_kendaraan_print_excel()
  {
    $start_date = $this->input->get("start_date") != NULL ? $this->input->get("start_date") : FALSE;
    $end_date = $this->input->get("end_date") != NULL ? $this->input->get("end_date") : FALSE;

    $data = [
      "list_kendaraan" => $this->model_kendaraan->get_kendaraan_list($start_date, $end_date),
    ];

    $this->load->view("admin/kendaraan/v_print_excel", $data);
  }

  public function process_kendaraan_delete($kendaraan_id)
  {
    $this->check_access("006KD", TRUE);

    $id = decrypt_url($kendaraan_id);
    $get_count = $this->model_kendaraan->get_count_kendaraan_service_by_kendaraan($id);

    if (count($get_count) == 0) {
      $this->model_kendaraan->delete_kendaraan($id);
      $this->session->set_flashdata("pesan", "<script>sweet('success', 'Berhasil!', 'Kendaraan berhasil dihapus')</script>");
    } else {
      $this->session->set_flashdata("pesan", "<script>sweet('error', 'Gagal!', 'Kendaraan ini memiliki service')</script>");
    }

    redirect("kendaraan");
  }
}