<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Merk extends CR_Controller
{
  public function __construct()
  {
    parent::__construct();
    $protect_login = $this->Auth->protect_login();
    $this->load->model("M_Merk", "model_merk");
    if ($protect_login->success === FALSE) {
      $this->session->set_flashdata("pesan", "<script>sweet('error', 'Gagal!', '$protect_login->message')</script>");
      redirect("login");
      exit;
    }
  }

  public function view_merk_management()
  {
    $this->check_access("004M", TRUE);

    $data = [
      "list_merk" => $this->model_merk->get_merk_list(),
      "allowed_add" => $this->check_access("004MA"),
      "allowed_edit" => $this->check_access("004ME"),
      "allowed_delete" => $this->check_access("004MD"),
    ];

    $this->view("admin/merk/v_index", "Merk Kendaraan", $data, TRUE);
  }

  public function process_merk_add()
  {
    $this->check_access("004MA", TRUE);
    
    $input = (object) html_escape($this->input->post());
    $this->model_merk->add_merk(["merk" => $input->merk]);

    $this->session->set_flashdata("pesan", "<script>sweet('success', 'Berhasil!', 'Merk kendaraan berhasil ditambahkan')</script>");
    redirect("merk-kendaraan");
  }

  public function process_merk_edit()
  {
    $this->check_access("004ME", TRUE);

    $input = (object) html_escape($this->input->post());
    $merk_id = decrypt_url($input->merk_id);
    $this->model_merk->update_merk($merk_id, ["merk" => $input->merk]);

    $this->session->set_flashdata("pesan", "<script>sweet('success', 'Berhasil!', 'Merk kendaraan berhasil diubah')</script>");
    redirect("merk-kendaraan");
  }

  public function process_merk_delete($merk_id)
  {
    $this->check_access("004MD", TRUE);

    $id = decrypt_url($merk_id);
    $get_count = $this->model_merk->get_count_kendaraan_by_merk($id);

    if (count($get_count) == 0) {
      $this->model_merk->delete_merk($id);
      $this->session->set_flashdata("pesan", "<script>sweet('success', 'Berhasil!', 'Merk kendaraan berhasil dihapus!')</script>");
    } else {
      $this->session->set_flashdata("pesan", "<script>sweet('error', 'Gagal!', 'Terdapat kendaraan yang menggunakan merk ini!')</script>");
    }

    redirect("merk-kendaraan");
  }
}