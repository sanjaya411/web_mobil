<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = 'welcome/view_404_page';
$route['translate_uri_dashes'] = FALSE;

$route["login"] = "Auth/view_login_page";
$route["validation_login"] = "Auth/process_validation_login";

$route["dashboard"] = "Admin/view_dashboard_page";
$route["logout"] = "Auth/process_logout";
$route["forbidden"] = "Auth/view_forbidden";

// Admin Management
$route["admin_management"] = "Admin/view_admin_management";
$route["get_admin"] = "Admin/get_admin_listed";
$route["add_admin"] = "Admin/validate_admin_add";
$route["admin_detail"] = "Admin/view_admin_detail";
$route["edit_admin"] = "Admin/process_admin_edit";

// UAC Management
$route["uac-management"] = "UAC/view_uac_management";
$route["uac-management/(:any)"] = "UAC/view_uac_detail/$1";
$route["uac/create-position"] = "UAC/validate_position_add";
$route["uac/create-division"] = "UAC/validate_division_add";
$route["uac/(:any)/assign"] = "UAC/process_position_assign/$1";

// Jenis Kendaraan
$route["jenis-kendaraan"] = "Jenis/view_jenis_management";
$route["jenis-kendaraan/create-jenis"] = "Jenis/process_jenis_add";
$route["jenis-kendaraan/edit-jenis"] = "Jenis/process_jenis_edit";
$route["jenis-kendaraan/delete-jenis/(:any)"] = "Jenis/process_jenis_delete/$1";

// Merk Kendaraan
$route["merk-kendaraan"] = "Merk/view_merk_management";
$route["merk-kendaraan/create-merk"] = "Merk/process_merk_add";
$route["merk-kendaraan/edit-merk"] = "Merk/process_merk_edit";
$route["merk-kendaraan/delete-merk/(:any)"] = "Merk/process_merk_delete/$1";

// Service Kendaraan
$route["service"] = "Service/view_service_management";
$route["service/create-service"] = "Service/process_service_add";
$route["service/edit-service"] = "Service/process_service_edit";
$route["service/delete-service/(:any)"] = "Service/process_service_delete/$1";

// Kendaraan
$route["kendaraan"] = "Kendaraan/view_kendaraan_management";
$route["kendaraan/create"] = "Kendaraan/view_kendaraan_add";
$route["kendaraan/create-kendaraan"] = "Kendaraan/validate_kendaraan_add";
$route["kendaraan/edit/(:any)"] = "Kendaraan/view_kendaraan_edit/$1";
$route["kendaraan/edit-kendaraan"] = "Kendaraan/validate_kendaraan_edit";
$route["kendaraan/delete/(:any)"] = "Kendaraan/process_kendaraan_delete/$1";
$route["kendaraan/print-pdf"] = "Kendaraan/view_kendaraan_print_pdf";
$route["kendaraan/print-excel"] = "Kendaraan/view_kendaraan_print_excel";

// Service Kendaraan
$route["service-kendaraan"] = "ServiceKendaraan/view_service_kendaraan_management";
$route["service-kendaraan/create"] = "ServiceKendaraan/view_service_kendaraan_add";
$route["service-kendaraan/create-service-kendaraan"] = "ServiceKendaraan/validate_service_kendaraan_add";
$route["service-kendaraan/edit/(:any)"] = "ServiceKendaraan/view_service_kendaraan_edit/$1";
$route["service-kendaraan/edit-service-kendaraan"] = "ServiceKendaraan/validate_service_kendaraan_edit";
$route["service-kendaraan/delete/(:any)"] = "ServiceKendaraan/view_service_kendaraan_delete/$1";
$route["service-kendaraan/print-pdf"] = "ServiceKendaraan/view_service_kendaraan_print_pdf";
$route["service-kendaraan/print-excel"] = "ServiceKendaraan/view_service_kendaraan_print_excel";